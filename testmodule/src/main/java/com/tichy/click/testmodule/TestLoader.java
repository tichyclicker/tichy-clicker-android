package com.tichy.click.testmodule;

import com.google.auto.service.AutoService;
import com.tichy.click.base.MainActivity;
import com.tichy.click.base.ModuleLoader;

@AutoService(ModuleLoader.class)
public class TestLoader implements ModuleLoader {
    @Override public void load(MainActivity mainActivity) {
        System.out.println("HI");
    }
}
