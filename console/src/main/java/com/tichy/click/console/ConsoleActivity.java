package com.tichy.click.console;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

public class ConsoleActivity extends AppCompatActivity {

    ConsoleViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_console);

        RecyclerView recyclerView = findViewById(R.id.log_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ConsoleViewAdapter(this, DebugConsoleWriter.getDebugConsoleWriter().getMessages());
        recyclerView.setAdapter(adapter);
        DebugConsoleWriter.getDebugConsoleWriter().registerAdapter(adapter);
        adapter.setClickListener((v, p) -> DebugConsoleWriter.getDebugConsoleWriter().write("MESSAGE_CLICKED", "Clicked message " + p));
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        DebugConsoleWriter.getDebugConsoleWriter().unregisterAdapter(adapter);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(data);
    }
}
