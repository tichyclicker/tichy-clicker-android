package com.tichy.click.console;

import com.tichy.click.base.ConsoleWriter;

import java.util.ArrayList;

public class DebugConsoleWriter implements ConsoleWriter {
    private static DebugConsoleWriter consoleWriter;

    private ArrayList<ConsoleMessage> messages;

    private ArrayList<ConsoleViewAdapter> adapters;

    private DebugConsoleWriter() {
        this.messages = new ArrayList<>();
        this.adapters = new ArrayList<>();
        write("STARTUP", "Registered new console!...");
    }

    public static DebugConsoleWriter getDebugConsoleWriter() {
        if (consoleWriter == null) {
            consoleWriter = new DebugConsoleWriter();
        }
        return consoleWriter;
    }

    @Override public void write(final String tag, final String message) {
        this.messages.add(new ConsoleMessage(tag, message));
        for (ConsoleViewAdapter adapter : adapters) {
            adapter.notifyDataSetChangedSafe();
        }
    }

    @Override public void setMessages(ArrayList<ConsoleMessage> messages) {
        this.messages = messages;
    }

    public void registerAdapter(final ConsoleViewAdapter adapter) {
        this.adapters.add(adapter);
    }

    public void unregisterAdapter(final ConsoleViewAdapter adapter) {
        this.adapters.remove(adapter);
    }

    public ArrayList<ConsoleMessage> getMessages() {
        return messages;
    }
}
