package com.tichy.click.console;

import android.content.Intent;

import com.google.auto.service.AutoService;
import com.tichy.click.base.MainActivity;
import com.tichy.click.base.ModuleLoader;

@AutoService(ModuleLoader.class)
public class Loader implements ModuleLoader {
    @Override public void load(final MainActivity mainActivity) {
        mainActivity.registerConsoleWriter(DebugConsoleWriter.getDebugConsoleWriter());
        Intent intent = new Intent();
        intent.setClass(mainActivity.getApplicationContext(), ConsoleActivity.class);
        //mainActivity.extStartActivity(intent);
    }
}
