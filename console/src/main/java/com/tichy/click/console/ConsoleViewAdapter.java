package com.tichy.click.console;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ConsoleViewAdapter extends RecyclerView.Adapter<ConsoleViewAdapter.ConsoleViewHolder> {
    private List<ConsoleMessage> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    ConsoleViewAdapter(final Context context, final List<ConsoleMessage> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @NotNull @Override
    public ConsoleViewHolder onCreateViewHolder(@NotNull final ViewGroup parent, final int viewType) {
        View view = mInflater.inflate(R.layout.console_row, parent, false);
        return new ConsoleViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ConsoleViewHolder holder, int position) {
        ConsoleMessage message = mData.get(position);
        holder.setValues(message);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void notifyDataSetChangedSafe() {
        new Handler(Looper.getMainLooper()).post(super::notifyDataSetChanged);
    }

    // stores and recycles views as they are scrolled off screen
    public class ConsoleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView date;
        TextView tag;
        TextView message;
        DateFormat format;

        ConsoleViewHolder(final View itemView) {
            super(itemView);
            this.date = itemView.findViewById(R.id.console_date);
            this.tag = itemView.findViewById(R.id.console_tag);
            this.message = itemView.findViewById(R.id.console_message);
            itemView.setOnClickListener(this);
            format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.getDefault());
            format = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
        }

        public void setValues(final ConsoleMessage message) {
            this.date.setText(format.format(message.getTimestamp()));
            this.tag.setText(message.getTag());
            this.message.setText(message.getMessage());
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    ConsoleMessage getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
