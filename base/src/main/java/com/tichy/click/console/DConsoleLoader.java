package com.tichy.click.console;

import android.util.Log;

import com.google.auto.service.AutoService;
import com.tichy.click.base.MainActivity;
import com.tichy.click.base.ModuleLoader;

@AutoService(ModuleLoader.class)
public class DConsoleLoader implements ModuleLoader {
    @Override public void load(MainActivity mainActivity) {
        Log.e("LOADER", "This is from DConsoleLoader");
    }
}
