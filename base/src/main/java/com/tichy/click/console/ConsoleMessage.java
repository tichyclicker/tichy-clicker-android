package com.tichy.click.console;

import java.util.Date;

public class ConsoleMessage {
    private String tag;
    private String message;
    private Date timestamp;

    public ConsoleMessage(final String tag, final String message) {
        this.tag = tag;
        this.message = message;
        this.timestamp = new Date(System.currentTimeMillis());
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
