package com.tichy.click.base;

import com.tichy.click.base.items.ShopItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ItemsUpdater {
    private MainActivity context;
    private String jsonURL;

    public ItemsUpdater(final MainActivity context) {
        this.context = context;
        this.jsonURL = context.getString(R.string.shops_url);
    }

    public void update() {
        runOnNewThread(this::updateInternal);
    }

    private void updateInternal() {
        JSONObject object = downloadJSON();
        if (object == null) {
            return;
        }
        try {
            JSONObject systems = object.getJSONObject("systems");
            JSONObject skills = object.getJSONObject("skills");
            JSONArray systems_items = systems.getJSONArray("items");
            JSONArray skills_items = skills.getJSONArray("items");
            for (int i = 0; i < systems_items.length(); i++) {
                JSONObject item = systems_items.getJSONObject(i);
                String key = item.getString("key");
                String displayText = item.getString("displayText");
                String description = item.getString("description");
                double initialCost = item.getDouble("initialCost");
                double costFactor = item.getDouble("costFactor");
                double income = item.getDouble("income");
                int bought = 0;
                ShopItem toAdd = new ShopItem(key, displayText, description, initialCost, costFactor, income, bought);
                synchronized (this.context.getSkillsShop()) {
                    if (!this.context.getSkillsShop().hasItem(key)) {
                        this.context.getSkillsShop().getItems().add(toAdd);
                    }
                }
            }
            for (int i = 0; i < skills_items.length(); i++) {
                JSONObject item = skills_items.getJSONObject(i);
                String key = item.getString("key");
                String displayText = item.getString("displayText");
                String description = item.getString("description");
                double initialCost = item.getDouble("initialCost");
                double costFactor = item.getDouble("costFactor");
                double income = item.getDouble("income");
                int bought = 0;
                ShopItem toAdd = new ShopItem(key, displayText, description, initialCost, costFactor, income, bought);
                synchronized (this.context.getSkillsShop()) {
                    if (!this.context.getSystemsShop().hasItem(key)) {
                        this.context.getSystemsShop().getItems().add(toAdd);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject downloadJSON() {
        JSONObject object = null;
        try {
            URL url = new URL(this.jsonURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(connection.getInputStream());
            String rawString = inputStreamToString(in);
            connection.disconnect();
            object = new JSONObject(rawString);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    private String inputStreamToString(final InputStream inputStream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        for (String line; (line = r.readLine()) != null; ) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    private void runOnUiThread(final Runnable runnable) {
        this.context.runOnUiThread(runnable);
    }

    private void runOnNewThread(final Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
