package com.tichy.click.base;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.play.core.splitinstall.SplitInstallManager;
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory;
import com.google.android.play.core.splitinstall.SplitInstallRequest;
import com.tichy.click.base.CommitsContract.Commits;
import com.tichy.click.base.items.Shop;
import com.tichy.click.base.items.ShopItemAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Set;

/**
 * Main Activity of the Application.
 *
 * @author Raphael Hahn
 * @version 0.0.0.4
 * @since 0.0.0.1
 */
public class MainActivity extends AppCompatActivity {

    /**
     * The Tichy image.
     *
     * @since 0.0.0.4
     */
    private ImageView tichyImage;

    /**
     * The commits text.
     *
     * @since 0.0.0.4
     */
    private TextView commitsText;

    /**
     * The commits per second text.
     *
     * @since 0.0.0.4
     */
    private TextView commitsPerSecondText;

    /**
     * The commits.
     *
     * @since 0.0.0.4
     */
    private double commits;

    /**
     * The commits per second.
     *
     * @since 0.0.0.4
     */
    private double commitsPerSecond;

    /**
     * The commits per click.
     *
     * @since 0.0.0.4
     */
    private double commitsPerClick;

    /**
     * The interval to update the commits.
     *
     * @since 0.0.0.4
     */
    private int updateInterval;

    /**
     * The handler for the commits per second.
     *
     * @since 0.0.0.4
     */
    private Handler secondsHandler;

    /**
     * The console writer to write debug information to.
     *
     * @since 0.0.0.4
     */
    private ConsoleWriter consoleWriter;

    /**
     * The shop where to buy skills.
     *
     * @since 0.0.0.4
     */
    private Shop skillsShop;

    /**
     * The shop where to buy systems.
     *
     * @since 0.0.0.4
     */
    private Shop systemsShop;

    private RecyclerView skillsView;

    private RecyclerView systemsView;

    private ShopItemAdapter skillsAdapter;

    private ShopItemAdapter systemsAdapter;

    private ItemsUpdater itemsUpdater;

    private TextView title;

    /**
     * The updater for the commits per second.
     *
     * @since 0.0.0.4
     */
    private Runnable secondsUpdater = new Runnable() {
        @Override
        public void run() {
            try {
                onTichySecond();
            } finally {
                secondsHandler.postDelayed(secondsUpdater, updateInterval);
            }
        }
    };

    /**
     * The database.
     *
     * @since 0.0.0.4
     */
    private SQLiteDatabase db;

    /**
     * Manager to download modules.
     *
     * @since 0.0.0.4
     */
    private SplitInstallManager splitInstallManager;

    /**
     * Function that gets called when the {@link AppCompatActivity} is created. Used to setup all teh necessary data,
     * listeners and so on.
     *
     * @param savedInstanceState the saved instance state
     *
     * @since 0.0.0.1
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.consoleWriter = new BasicConsoleWriter();

        this.splitInstallManager = SplitInstallManagerFactory.create(this);

        this.loadViews();
        this.initValues();
        this.initViews();
        this.initSecondHandler();
        installConsole();
        this.loadModules();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String url = bundle.getString("url", null);
            if (url != null) {
                this.consoleWriter.write("UPDATE", url);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
        this.initShops();
    }

    /**
     * Installs the settings module if it is not installed.
     *
     * @since 0.0.0.4
     */
    private void installConsole() {
        if (this.splitInstallManager.getInstalledModules().contains("console")) {
            this.consoleWriter.write("MODULE_INSTALLER", "Console is already installed!");
        } else {
            SplitInstallRequest request = SplitInstallRequest.newBuilder().addModule("console").build();
            this.splitInstallManager.startInstall(request)
                                    .addOnCompleteListener(
                                            e -> this.consoleWriter.write("MODULE_INSTALLER", e.toString()))
                                    .addOnFailureListener(
                                            e -> this.consoleWriter.write("MODULE_INSTALLER", e.toString()))
                                    .addOnSuccessListener(
                                            e -> this.consoleWriter.write("MODULE_INSTALLER", e.toString()));
            this.splitInstallManager.registerListener(s -> this.consoleWriter.write("MODULE_INSTALLER",
                                                                                    s.bytesDownloaded() + " / " +
                                                                                    s.totalBytesToDownload() +
                                                                                    " bytes downloaded"));
        }

    }

    /**
     * Animation when a new finger presses down on the {@link MainActivity#tichyImage}.
     *
     * @param view         the view that should be animated
     * @param pointerCount the count of fingers pressing on the screen
     *
     * @since 0.0.0.4
     */
    public void onTichyDown(final View view, final int pointerCount) {
        view.setScaleX(1 - ((float) pointerCount * 0.05f));
        view.setScaleY(1 - ((float) pointerCount * 0.05f));
        view.setElevation((float) pointerCount * 5f);
    }

    /**
     * Animation when a finger stops pressing on the {@link MainActivity#tichyImage}.
     *
     * @param view         the view that should be animated
     * @param pointerCount the count of fingers pressing on the screen before one finger was released
     *
     * @since 0.0.0.4
     */
    public void onTichyUp(final View view, final int pointerCount) {
        view.setScaleX(1 - ((float) (pointerCount - 1) * 0.05f));
        view.setScaleY(1 - ((float) (pointerCount - 1) * 0.05f));
        view.setElevation((float) (pointerCount - 1) * 5f);
        this.onTichyClick(view);
    }

    /**
     * Manage the click on the {@link MainActivity#tichyImage}. Adds the {@link MainActivity#commitsPerClick} to the
     * {@link MainActivity#commits}
     *
     * @param view the view that has been clicked on
     *
     * @since 0.0.0.4
     */
    public void onTichyClick(final View view) {
        if (view.getId() == R.id.imageTichy) {
            this.commits += this.commitsPerClick;
            this.updateTexts();
        }
    }

    /**
     * Handles clicks on the buy systems button.
     *
     * @param view the clicked button
     *
     * @since 0.0.0.4
     */
    public void onBuySystemsClick(final View view) {
        Toast.makeText(this, "Buy systems", Toast.LENGTH_SHORT).show();
    }

    /**
     * Handles clicks on the buy skills button.
     *
     * @param view the clicked button
     *
     * @since 0.0.0.4
     */
    public void onBuySkillsClick(final View view) {
        Toast.makeText(this, "Buy skills", Toast.LENGTH_SHORT).show();
    }

    /**
     * Count Up the commits by commits per second.
     *
     * @since 0.0.0.4
     */
    public void onTichySecond() {
        this.commits += this.commitsPerSecond;
        this.runOnUiThread(this::updateTexts);
    }

    /**
     * Loads all needed references to the views in {@link MainActivity}.
     *
     * @since 0.0.0.4
     */
    private void loadViews() {
        this.tichyImage = findViewById(R.id.imageTichy);
        this.commitsText = findViewById(R.id.textCommitCount);
        this.commitsPerSecondText = findViewById(R.id.textCommitsPerSecond);
        this.skillsView = findViewById(R.id.list_skills);
        this.systemsView = findViewById(R.id.list_systems);
        this.title = findViewById(R.id.title);
    }

    /**
     * Initialises all values needed.
     *
     * @since 0.0.0.4
     */
    private void initValues() {
        this.initDB();
        this.commits = readCommits();
        this.commitsPerSecond = readCommitsPerSecond();
        this.commitsPerClick = readCommitsPerClick();
        this.updateInterval = 1000;
    }

    /**
     * Initialises the database.
     *
     * @since 0.0.0.4
     */
    private void initDB() {
        CommitsDbHelper dbHelper = new CommitsDbHelper(this);
        this.db = dbHelper.getWritableDatabase();
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + Commits.TABLE, null);
        if (cursor.getCount() < 1) {
            ContentValues values = new ContentValues();
            values.put(Commits.COLUMN_COMMITS, 0.0);
            values.put(Commits.COLUMN_COMMITS_PER_SECOND, 0.0);
            values.put(Commits.COLUMN_COMMITS_PER_CLICK, 1.0);
            values.put(Commits.COLUMN_TIME, System.currentTimeMillis());
            this.db.insert(Commits.TABLE, null, values);
        }
        cursor.close();
    }

    private void initShops() {
        this.skillsShop = new Shop("skills");
        this.systemsShop = new Shop("systems");

        this.skillsView.setLayoutManager(new LinearLayoutManager(this));
        this.skillsAdapter = new ShopItemAdapter(this, this.skillsShop.getItems());
        this.skillsView.setAdapter(this.skillsAdapter);

        this.systemsView.setLayoutManager(new LinearLayoutManager(this));
        this.systemsAdapter = new ShopItemAdapter(this, this.systemsShop.getItems());
        this.systemsView.setAdapter(this.systemsAdapter);

        this.itemsUpdater = new ItemsUpdater(this);
        this.itemsUpdater.update();

        //Todo add database
    }

    public void updateShops() {
        this.skillsAdapter.notifyDataSetChanged();
        this.systemsAdapter.notifyDataSetChanged();
    }

    /**
     * Writes the commits to the database.
     *
     * @param commits the commits
     *
     * @since 0.0.0.4
     */
    private void writeCommits(final double commits) {
        ContentValues values = new ContentValues();
        values.put(Commits.COLUMN_COMMITS, commits);
        values.put(Commits.COLUMN_TIME, System.currentTimeMillis());
        this.db.update(Commits.TABLE, values, null, null);
    }

    /**
     * Writes the commits per second to the database.
     *
     * @param commitsPerSecond the commits per second
     *
     * @since 0.0.0.4
     */
    private void writeCommitsPerSecond(final double commitsPerSecond) {
        ContentValues values = new ContentValues();
        values.put(Commits.COLUMN_COMMITS_PER_SECOND, commitsPerSecond);
        this.db.update(Commits.TABLE, values, null, null);
    }

    /**
     * Writes the commits per click to the database.
     *
     * @param commitsPerClick the commits per click
     *
     * @since 0.0.0.4
     */
    private void writeCommitsPerClick(final double commitsPerClick) {
        ContentValues values = new ContentValues();
        values.put(Commits.COLUMN_COMMITS_PER_CLICK, commitsPerClick);
        this.db.update(Commits.TABLE, values, null, null);
    }

    /**
     * Reads the current commits from the database and adds the meanwhile generated commits.
     *
     * @return the commits
     *
     * @since 0.0.0.4
     */
    private double readCommits() {
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + Commits.TABLE, null);
        cursor.moveToFirst();
        int commitsIndex = cursor.getColumnIndex(Commits.COLUMN_COMMITS);
        double commits = commitsIndex >= 0 ? cursor.getDouble(commitsIndex) : 0;
        int cpsIndex = cursor.getColumnIndex(Commits.COLUMN_COMMITS_PER_SECOND);
        double commitsPerSecond = cpsIndex >= 0 ? cursor.getDouble(cpsIndex) : 0;
        int timeIndex = cursor.getColumnIndex(Commits.COLUMN_TIME);
        long last_time = timeIndex >= 0 ? cursor.getLong(timeIndex) : System.currentTimeMillis();
        cursor.close();
        int seconds = (int) ((System.currentTimeMillis() - last_time) / 1000.0);
        commits += seconds * commitsPerSecond;
        return commits;
    }

    /**
     * Reads the commits per second.
     *
     * @return the commits per second
     *
     * @since 0.0.0.4
     */
    private double readCommitsPerSecond() {
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + Commits.TABLE, null);
        cursor.moveToFirst();
        int cpsIndex = cursor.getColumnIndex(Commits.COLUMN_COMMITS_PER_SECOND);
        double commitsPerSecond = cpsIndex >= 0 ? cursor.getDouble(cpsIndex) : 0;
        cursor.close();
        return commitsPerSecond;
    }

    /**
     * Reads the commits per click.
     *
     * @return the commits per click
     *
     * @since 0.0.0.4
     */
    private double readCommitsPerClick() {
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + Commits.TABLE, null);
        cursor.moveToFirst();
        int cpcIndex = cursor.getColumnIndex(Commits.COLUMN_COMMITS_PER_CLICK);
        double commitsPerClick = cpcIndex >= 0 ? cursor.getDouble(cpcIndex) : 0;
        cursor.close();
        return commitsPerClick;
    }

    /**
     * Initialises the loaded views withe the correct values and listeners.
     *
     * @since 0.0.0.4
     */
    private void initViews() {
        this.updateTexts();
        this.initTichyListeners();
        this.initShops();
        this.initTitle();
    }

    /**
     * Initialises the title with rainbow colors.
     *
     * @since 0.0.0.4
     */
    private void initTitle() {
        this.title.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Shader textShader = new LinearGradient(
                    0, 0, this.title.getMeasuredWidth(), 0,
                    new int[]{getResources().getColor(R.color.red, null),
                              getResources().getColor(R.color.orange, null),
                              getResources().getColor(R.color.yellow, null),
                              getResources().getColor(R.color.lemon, null),
                              getResources().getColor(R.color.green, null),
                              getResources().getColor(R.color.gyan, null),
                              getResources().getColor(R.color.cyan, null),
                              getResources().getColor(R.color.byan, null),
                              getResources().getColor(R.color.blue, null),
                              getResources().getColor(R.color.indigo, null),
                              getResources().getColor(R.color.violet, null),
                              getResources().getColor(R.color.red, null)},
                    new float[]{1.0f / 13.0f, 2.0f / 13.0f, 3.0f / 13.0f, 4.0f / 13.0f, 5.0f / 13.0f, 6.0f / 13.0f,
                                7.0f / 13.0f, 8.0f / 13.0f, 9.0f / 13.0f, 10.0f / 13.0f, 11.0f / 13.0f, 12.0f / 13.0f},
                    Shader.TileMode.CLAMP);
            this.title.getPaint().setShader(textShader);
        });
    }

    public float convertDpToPx(float dp) {
        return dp * this.getResources().getDisplayMetrics().density;
    }

    /**
     * Initialises the seconds handler.
     *
     * @since 0.0.0.4
     */
    private void initSecondHandler() {
        this.secondsHandler = new Handler();
        this.startSecondTask();
    }

    /**
     * Starts the seconds task.
     *
     * @since 0.0.0.4
     */
    void startSecondTask() {
        this.secondsUpdater.run();
    }

    /**
     * Stops the seconds task.
     *
     * @since 0.0.0.4
     */
    void stopSecondTask() {
        this.secondsHandler.removeCallbacks(this.secondsUpdater);
    }

    /**
     * Stops the seconds task.
     *
     * @since 0.0.0.4
     */
    @Override protected void onDestroy() {
        super.onDestroy();
        this.stopSecondTask();
        this.writeCommits(this.commits);
        this.writeCommitsPerSecond(this.commitsPerSecond);
        this.db.close();
    }

    /**
     * Updates the texts.
     *
     * @since 0.0.0.4
     */
    private void updateTexts() {
        String commitsPerSecondText =
                this.commitsPerSecond + "/s, " + this.commitsPerClick + "/" + getResources().getString(R.string.click);
        this.commitsPerSecondText.setText(commitsPerSecondText);
        String commitsText = this.commits + " " + getResources().getString(R.string.commits);
        this.commitsText.setText(commitsText);
    }

    /**
     * Initialises all listeners to the corresponding views.
     *
     * @since 0.0.0.4
     */
    private void initTichyListeners() {
        this.initTichyClickListener();
    }

    /**
     * Registers the console writer.
     *
     * @param consoleWriter the console writer
     *
     * @since 0.0.0.4
     */
    public void registerConsoleWriter(final ConsoleWriter consoleWriter) {
        consoleWriter.setMessages(this.consoleWriter.getMessages());
        this.consoleWriter = consoleWriter;
    }

    /**
     * Loads all required modules.
     *
     * @since 0.0.0.4
     */
    private void loadModules() {
        Set<String> modules = this.splitInstallManager.getInstalledModules();

        /*for (String module : modules) {
            try {
                Class<?> rawFeature = Class.forName("com.tichy.click." + module + ".Loader");
                Object rawObject = rawFeature.newInstance();
                if (rawObject instanceof ModuleLoader) {
                    ModuleLoader loader = (ModuleLoader) rawObject;
                    loader.load(this);
                }
            } catch (ClassNotFoundException | IllegalAccessException |
                    InstantiationException e) {
                e.printStackTrace();
            }
        }*/

        /*try {
            Class<?> rawFeature = Class.forName("com.tichy.click.FBRegistrator");
            Object rawObject = rawFeature.newInstance();
            if (rawObject instanceof Registrator) {
                Registrator registrator = (Registrator) rawObject;
                registrator.register();
            }
        } catch (ClassNotFoundException | IllegalAccessException |
                InstantiationException e) {
            e.printStackTrace();
        }*/

        ArrayList<String> classNames = new ArrayList<>();
        ArrayList<Class<? extends ModuleLoader>> moduleLoaders = new ArrayList<>();
        try {
            Enumeration<URL> resources = Objects.requireNonNull(getClass().getClassLoader())
                                                .getResources("META-INF/services/com.tichy.click.base.ModuleLoader");
            while (resources.hasMoreElements()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(resources.nextElement().openStream()));
                String line = reader.readLine();
                while (line != null) {
                    if (!classNames.contains(line)) {
                        classNames.add(line);
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        for (String className : classNames) {
            try {
                Class<? extends ModuleLoader> c = Class.forName(className).asSubclass(ModuleLoader.class);
                if (!moduleLoaders.contains(c)) {
                    moduleLoaders.add(c);
                }
                this.consoleWriter.write("IS INSTALLED", className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                this.consoleWriter.write("NOT INSTALLED", className);
            }
        }

        for (Class<? extends ModuleLoader> moduleLoader : moduleLoaders) {
            this.consoleWriter.write("LOADING", moduleLoader.getName());
            try {
                ModuleLoader loader = moduleLoader.newInstance();
                loader.load(this);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Starts a new activity.
     *
     * @param intent The intent to execute
     *
     * @since 0.0.0.4
     */
    public void extStartActivity(final Intent intent) {
        this.startActivity(intent);
    }

    public Shop getSkillsShop() {
        return skillsShop;
    }

    public Shop getSystemsShop() {
        return systemsShop;
    }

    public ConsoleWriter getConsoleWriter() {
        return consoleWriter;
    }

    /**
     * Sets the {@link MainActivity#tichyImage} {@link android.view.View.OnTouchListener} to handle every down and up
     * action with the corresponding onTichy function.
     *
     * @since 0.0.0.4
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initTichyClickListener() {
        this.tichyImage.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.imageTichy) {
                // Tests for ten different fingers due to errors in api
                // In future only actions down and pointer down should work
                if (event.getAction() == MotionEvent.ACTION_DOWN
                    | event.getAction() == MotionEvent.ACTION_POINTER_DOWN
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x100)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x200)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x300)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x400)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x500)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x600)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x700)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x800)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x900)) {
                    this.onTichyDown(view, event.getPointerCount());
                    // Same problem as with down
                } else if (event.getAction() == MotionEvent.ACTION_UP
                           | event.getAction() == MotionEvent.ACTION_POINTER_UP
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x100)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x200)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x300)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x400)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x500)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x600)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x700)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x800)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x900)) {
                    this.onTichyUp(view, event.getPointerCount());
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    this.onTichyUp(view, 1);
                }
            }
            return false;
        });
    }
}
