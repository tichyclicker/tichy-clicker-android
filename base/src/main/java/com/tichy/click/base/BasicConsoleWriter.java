package com.tichy.click.base;

import android.util.Log;

import com.tichy.click.console.ConsoleMessage;

import java.util.ArrayList;

public class BasicConsoleWriter implements ConsoleWriter {
    private ArrayList<ConsoleMessage> messages;

    BasicConsoleWriter() {
        this.messages = new ArrayList<>();
    }

    @Override public void write(final String tag, final String message) {
        Log.d(tag, message);
        this.messages.add(new ConsoleMessage(tag, message));
    }

    @Override public void setMessages(ArrayList<ConsoleMessage> messages) {
        this.messages = messages;
    }

    @Override public ArrayList<ConsoleMessage> getMessages() {
        return this.messages;
    }
}
