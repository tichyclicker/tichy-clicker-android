package com.tichy.click.base.items;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tichy.click.base.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ShopItemAdapter extends RecyclerView.Adapter<ShopItemAdapter.ShopItemViewHolder> {
    private List<ShopItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public ShopItemAdapter(Context context, List<ShopItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @NotNull @Override
    public ShopItemViewHolder onCreateViewHolder(@NotNull final ViewGroup parent, final int viewType) {
        View view = mInflater.inflate(R.layout.recycler_view_item, parent, false);
        return new ShopItemViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ShopItemViewHolder holder, final int position) {
        ShopItem item = mData.get(position);
        holder.setValues(item);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ShopItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView description;
        TextView cost;

        ShopItemViewHolder(final View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.shop_item_name);
            this.description = itemView.findViewById(R.id.shop_item_description);
            this.cost = itemView.findViewById(R.id.shop_item_cost);
            itemView.setOnClickListener(this);
        }

        public void setValues(final ShopItem item) {
            this.name.setText(item.getDisplayText());
            this.description.setText(item.getDescription());
            this.cost.setText(
                    String.format("%s %s", item.getInitialCost() * (Math.pow(item.getCostFactor(), item.getBought())),
                                  name.getContext().getResources().getString(R.string.commits)));
        }

        //Todo add buy function
        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    private ShopItem getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
