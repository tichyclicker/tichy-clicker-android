package com.tichy.click.base;

import android.provider.BaseColumns;

public class CommitsContract {

    private CommitsContract() {
    }

    /* Inner class that defines the table contents */
    public static class Commits implements BaseColumns {
        public static final String TABLE = "commits";
        public static final String COLUMN_COMMITS = "commits";
        public static final String COLUMN_COMMITS_PER_SECOND = "commits_per_second";
        public static final String COLUMN_COMMITS_PER_CLICK = "commits_per_click";
        public static final String COLUMN_TIME = "time";
    }
}
