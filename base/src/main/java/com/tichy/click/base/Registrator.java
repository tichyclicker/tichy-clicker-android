package com.tichy.click.base;

public interface Registrator {
    void register();
}
