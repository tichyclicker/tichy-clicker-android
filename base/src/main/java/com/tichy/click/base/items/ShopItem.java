package com.tichy.click.base.items;

public class ShopItem {
    private String key;
    private String displayText;
    private String description;
    private double initialCost;
    private double costFactor;
    private double income;
    private int bought;

    public ShopItem(final String key,
                    final String displayText,
                    final String description,
                    final double initialCost,
                    final double costFactor,
                    final double income,
                    final int bought) {
        this.key = key;
        this.displayText = displayText;
        this.description = description;
        this.initialCost = initialCost;
        this.costFactor = costFactor;
        this.income = income;
        this.bought = bought;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getDescription() {
        return description;
    }

    public double getInitialCost() {
        return initialCost;
    }

    public double getCostFactor() {
        return costFactor;
    }

    public double getIncome() {
        return income;
    }

    public int getBought() {
        return bought;
    }

    public void setBought(int bought) {
        this.bought = bought;
    }
}
