package com.tichy.click.base;

import com.tichy.click.console.ConsoleMessage;

import java.util.ArrayList;

public interface ConsoleWriter {
    void write(final String tag, final String message);

    void setMessages(final ArrayList<ConsoleMessage> messages);

    ArrayList<ConsoleMessage> getMessages();
}
