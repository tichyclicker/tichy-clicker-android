package com.tichy.click.base;

public interface ModuleLoader {
    void load(final MainActivity mainActivity);
}
