package com.tichy.click.base.items;

import java.util.ArrayList;

public class Shop {
    private ArrayList<ShopItem> items;
    private String name;

    public Shop(final String name) {
        this.items = new ArrayList<>();
        this.name = name;
    }

    public ArrayList<ShopItem> getItems() {
        return items;
    }

    public boolean hasItem(final String key) {
        for (final ShopItem item : this.items) {
            if (item.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }
}
