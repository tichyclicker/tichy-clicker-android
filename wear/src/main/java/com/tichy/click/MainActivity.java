package com.tichy.click;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends WearableActivity {
    /**
     * The Tichy image.
     *
     * @since 0.0.0.4
     */
    private ImageView tichyImage;

    /**
     * The commits text.
     *
     * @since 0.0.0.4
     */
    private TextView commitsText;

    /**
     * The commits per second text.
     *
     * @since 0.0.0.4
     */
    private TextView commitsPerSecondText;

    /**
     * The commits.
     *
     * @since 0.0.0.4
     */
    private int commits;

    /**
     * The commits per second.
     *
     * @since 0.0.0.4
     */
    private int commitsPerSecond;

    /**
     * The commits per click.
     *
     * @since 0.0.0.4
     */
    private int commitsPerClick;

    /**
     * The interval to update the commits.
     *
     * @since 0.0.0.4
     */
    private int updateInterval;

    /**
     * The handler for the commits per second.
     *
     * @since 0.0.0.4
     */
    private Handler secondsHandler;

    /**
     * The updater for the commits per second.
     *
     * @since 0.0.0.4
     */
    private Runnable secondsUpdater = new Runnable() {
        @Override
        public void run() {
            try {
                onTichySecond();
            } finally {
                secondsHandler.postDelayed(secondsUpdater, updateInterval);
            }
        }
    };

    /**
     * The database.
     *
     * @since 0.0.0.4
     */
    private SQLiteDatabase db;

    /**
     * Function that gets called when the {@link MainActivity} is created. Used to setup all teh necessary data,
     * listeners and so on.
     *
     * @param savedInstanceState the saved instance state
     *
     * @since 0.0.0.1
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.loadViews();
        this.initValues();
        this.initViews();
        this.initSecondHandler();
        this.setAmbientEnabled();
    }

    /**
     * Animation when a new finger presses down on the {@link MainActivity#tichyImage}.
     *
     * @param view         the view that should be animated
     * @param pointerCount the count of fingers pressing on the screen
     *
     * @since 0.0.0.4
     */
    public void onTichyDown(final View view, final int pointerCount) {
        view.setScaleX(1 - ((float) pointerCount * 0.05f));
        view.setScaleY(1 - ((float) pointerCount * 0.05f));
        view.setElevation((float) pointerCount * 5f);
    }

    /**
     * Animation when a finger stops pressing on the {@link MainActivity#tichyImage}.
     *
     * @param view         the view that should be animated
     * @param pointerCount the count of fingers pressing on the screen before one finger was released
     *
     * @since 0.0.0.4
     */
    public void onTichyUp(final View view, final int pointerCount) {
        view.setScaleX(1 - ((float) (pointerCount - 1) * 0.05f));
        view.setScaleY(1 - ((float) (pointerCount - 1) * 0.05f));
        view.setElevation((float) (pointerCount - 1) * 5f);
        this.onTichyClick(view);
    }

    /**
     * Manage the click on the {@link MainActivity#tichyImage}. Adds the {@link MainActivity#commitsPerClick} to the
     * {@link MainActivity#commits}
     *
     * @param view the view that has been clicked on
     *
     * @since 0.0.0.4
     */
    public void onTichyClick(final View view) {
        if (view.getId() == R.id.imageTichy) {
            this.commits += this.commitsPerClick;
            this.updateTexts();
        }
    }

    /**
     * Count Up the commits by commits per second.
     *
     * @since 0.0.0.4
     */
    public void onTichySecond() {
        this.commits += this.commitsPerSecond;
        this.runOnUiThread(this::updateTexts);
    }

    /**
     * Loads all needed references to the views in {@link MainActivity}.
     *
     * @since 0.0.0.4
     */
    private void loadViews() {
        this.tichyImage = findViewById(R.id.imageTichy);
        this.commitsText = findViewById(R.id.textCommitCount);
        this.commitsPerSecondText = findViewById(R.id.textCommitsPerSecond);
    }

    /**
     * Initialises all values needed.
     *
     * @since 0.0.0.4
     */
    private void initValues() {
        this.initDB();
        this.commits = readCommits();
        this.commitsPerSecond = readCommitsPerSecond();
        this.commitsPerClick = 1;
        this.updateInterval = 1000;
    }

    /**
     * Initialises the database.
     *
     * @since 0.0.0.4
     */
    private void initDB() {
        CommitsDbHelper dbHelper = new CommitsDbHelper(this);
        this.db = dbHelper.getWritableDatabase();
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + CommitsContract.Commits.TABLE, null);
        if (cursor.getCount() < 1) {
            ContentValues values = new ContentValues();
            values.put(CommitsContract.Commits.COLUMN_COMMITS, 0);
            values.put(CommitsContract.Commits.COLUMN_COMMITS_PER_SECOND, 0);
            values.put(CommitsContract.Commits.COLUMN_TIME, System.currentTimeMillis());
            this.db.insert(CommitsContract.Commits.TABLE, null, values);
        }
        cursor.close();
    }

    /**
     * Writes the commits to the database.
     *
     * @param commits the commits
     *
     * @since 0.0.0.4
     */
    private void writeCommits(final int commits) {
        ContentValues values = new ContentValues();
        values.put(CommitsContract.Commits.COLUMN_COMMITS, commits);
        values.put(CommitsContract.Commits.COLUMN_TIME, System.currentTimeMillis());
        this.db.update(CommitsContract.Commits.TABLE, values, null, null);
    }

    /**
     * Writes the commits per second to the database.
     *
     * @param commitsPerSecond the commits per second
     *
     * @since 0.0.0.4
     */
    private void writeCommitsPerSecond(final int commitsPerSecond) {
        ContentValues values = new ContentValues();
        values.put(CommitsContract.Commits.COLUMN_COMMITS_PER_SECOND, commitsPerSecond);
        this.db.update(CommitsContract.Commits.TABLE, values, null, null);
    }

    /**
     * Reads the current commits from the database and adds the meanwhile generated commits.
     *
     * @return the commits
     *
     * @since 0.0.0.4
     */
    private int readCommits() {
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + CommitsContract.Commits.TABLE, null);
        cursor.moveToFirst();
        int commitsIndex = cursor.getColumnIndex(CommitsContract.Commits.COLUMN_COMMITS);
        int commits = commitsIndex >= 0 ? cursor.getInt(commitsIndex) : 0;
        int cpsIndex = cursor.getColumnIndex(CommitsContract.Commits.COLUMN_COMMITS_PER_SECOND);
        double commitsPerSecond = cpsIndex >= 0 ? cursor.getDouble(cpsIndex) : 0;
        int timeIndex = cursor.getColumnIndex(CommitsContract.Commits.COLUMN_TIME);
        long last_time = timeIndex >= 0 ? cursor.getLong(timeIndex) : System.currentTimeMillis();
        cursor.close();
        int seconds = (int) ((System.currentTimeMillis() - last_time) / 1000.0);
        commits += seconds * commitsPerSecond;
        return commits;
    }

    /**
     * Reads the commits per second.
     *
     * @return the commits per second
     *
     * @since 0.0.0.4
     */
    private int readCommitsPerSecond() {
        Cursor cursor = this.db.rawQuery("SELECT * FROM " + CommitsContract.Commits.TABLE, null);
        cursor.moveToFirst();
        int cpsIndex = cursor.getColumnIndex(CommitsContract.Commits.COLUMN_COMMITS_PER_SECOND);
        int commitsPerSecond = cpsIndex >= 0 ? cursor.getInt(cpsIndex) : 0;
        cursor.close();
        return commitsPerSecond;
    }

    /**
     * Initialises the loaded views withe the correct values and listeners.
     *
     * @since 0.0.0.4
     */
    private void initViews() {
        this.updateTexts();
        this.initTichyListeners();
    }

    /**
     * Initialises the seconds handler.
     *
     * @since 0.0.0.4
     */
    private void initSecondHandler() {
        secondsHandler = new Handler();
        startSecondTask();
    }

    /**
     * Starts the seconds task.
     *
     * @since 0.0.0.4
     */
    void startSecondTask() {
        secondsUpdater.run();
    }

    /**
     * Stops the seconds task.
     *
     * @since 0.0.0.4
     */
    void stopSecondTask() {
        secondsHandler.removeCallbacks(secondsUpdater);
    }

    /**
     * Stops the seconds task.
     *
     * @since 0.0.0.4
     */
    @Override protected void onDestroy() {
        super.onDestroy();
        stopSecondTask();
        writeCommits(this.commits);
        writeCommitsPerSecond(this.commitsPerSecond);
        db.close();
    }

    /**
     * Updates the texts.
     *
     * @since 0.0.0.4
     */
    private void updateTexts() {
        String commitsPerSecondText =
                this.commitsPerSecond + " " + getResources().getString(R.string.commits_per_second);
        this.commitsPerSecondText.setText(commitsPerSecondText);
        String commitsText = this.commits + " " + getResources().getString(R.string.commits);
        this.commitsText.setText(commitsText);
    }

    /**
     * Initialises all listeners to the corresponding views.
     *
     * @since 0.0.0.4
     */
    private void initTichyListeners() {
        this.initTichyClickListener();
    }

    /**
     * Sets the {@link MainActivity#tichyImage} {@link android.view.View.OnTouchListener} to handle every down and up
     * action with the corresponding onTichy function.
     *
     * @since 0.0.0.4
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initTichyClickListener() {
        this.tichyImage.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.imageTichy) {
                // Tests for ten different fingers due to errors in api
                // In future only actions down and pointer down should work
                if (event.getAction() == MotionEvent.ACTION_DOWN
                    | event.getAction() == MotionEvent.ACTION_POINTER_DOWN
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x100)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x200)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x300)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x400)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x500)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x600)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x700)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x800)
                    | event.getAction() == (MotionEvent.ACTION_POINTER_DOWN | 0x900)) {
                    this.onTichyDown(view, event.getPointerCount());
                    // Same problem as with down
                } else if (event.getAction() == MotionEvent.ACTION_UP
                           | event.getAction() == MotionEvent.ACTION_POINTER_UP
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x100)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x200)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x300)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x400)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x500)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x600)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x700)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x800)
                           | event.getAction() == (MotionEvent.ACTION_POINTER_UP | 0x900)) {
                    this.onTichyUp(view, event.getPointerCount());
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    this.onTichyUp(view, 1);
                }
            }
            return false;
        });
    }
}
