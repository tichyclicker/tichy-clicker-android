package com.tichy.click;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CommitsDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Commits.db";

    private static final String SQL_CREATE_COMMITS =
            "CREATE TABLE " + CommitsContract.Commits.TABLE + " (" +
            CommitsContract.Commits._ID + " INTEGER PRIMARY KEY," +
            CommitsContract.Commits.COLUMN_COMMITS + " INTEGER," +
            CommitsContract.Commits.COLUMN_COMMITS_PER_SECOND + " INTEGER," +
            CommitsContract.Commits.COLUMN_TIME + " INTEGER)";

    public CommitsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_COMMITS);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
